package pce.marko.aspnetandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/* import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient; */

public class MainActivity extends Activity {

    boolean flagZast = false;
    Button btns, btnt;

 // -----------------------------------
    private class HTTPAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return HttpGet(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            // - tvResult.setText(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    public boolean checkNetworkConnection() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isConnected = false;
        if (networkInfo != null && (isConnected = networkInfo.isConnected())) {
            // show "Connected" & type of network "WIFI or MOBILE"
            // - tvIsConnected.setText("Connected " + networkInfo.getTypeName());
            // change background color to red
            // - tvIsConnected.setBackgroundColor(0xFF7CCC26);


        } else {
            // show "Not Connected"
            // - tvIsConnected.setText("Not Connected");
            // change background color to green
            // - tvIsConnected.setBackgroundColor(0xFFFF0000);
        }

        return isConnected;
    }

    private String HttpGet(String myUrl) throws IOException {
        InputStream inputStream = null;
        String result = "";

        URL url = new URL(myUrl);

        // create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // make GET request to the given URL
        conn.connect();

        // receive response as inputStream
        inputStream = conn.getInputStream();

        // convert inputstream to string
        if(inputStream != null) {
            result = convertInputStreamToString(inputStream);
            Log.d("instreampovezi ", result);
        }
        else
            result = "Did not work!";

        return result;
    }
 // --------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btns = (Button)findViewById(R.id.button1);
        btnt = (Button)findViewById(R.id.button2);

        btnt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

             new HTTPAsyncTask().execute("http://gsmpodaci.gear.host/api/studentis");

            }
        });


         btns.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 URL url1;
                 HttpURLConnection conn1 = null;

                 InputStreamReader isReader = null;
                 // Read text into buffer.
                 BufferedReader bufReader = null;

            // !
                 InputStream inputStream = null;
                 String result = "";

                 URL url = null;
                 try {
                     url = new URL("http://gsmpodaci.gear.host/api/studentis");
                 } catch (MalformedURLException e) {
                     e.printStackTrace();
                 }

                 // create HttpURLConnection
                 HttpURLConnection conn = null;
                 try {
                     conn = (HttpURLConnection) url.openConnection();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }

                 // make GET request to the given URL
                 try {
                     conn.connect();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }

                 // receive response as inputStream
                 try {
                     inputStream = conn.getInputStream();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }


             }
         });

         Log.d("instream", "ulaz");


        if(isOnline(this)){
          Log.d("instream", "INTERNET VEZA USPOSTAVLJENA");
            new SendPostRequest().execute();
            new HTTPAsyncTask().execute("http://gsmpodaci.gear.host/api/studentis");
        }
        else{
            Log.d("instream", "INTERNET VEZA NIJE USPOSTAVLJENA, SPOJI SE !");
            showNoConnectionDialog(this);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
         if(isOnline(this)){
            Log.d("instream", "INTERNET VEZA USPOSTAVLJENA");
            new SendPostRequest().execute();
        }
        else{
            Log.d("instream", "INTERNET VEZA NIJE USPOSTAVLJENA, SPOJI SE !");
            showNoConnectionDialog(this);
        }*/
    }

    public class SendPostRequest extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
           // super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Ej", Toast.LENGTH_SHORT);
        }

        @Override
        protected String doInBackground(String... arg0) {
            Log.d("instream", "doInBackground");
            try{
                URL url = new URL("http://gsmpodaci.gear.host/api/studentis");

                JSONObject postDataParams = new JSONObject();
                 postDataParams.put("Name", "Woon Tan");
                 postDataParams.put("Email", "wt@ic.com");
                 postDataParams.put("Age", "30");
                Log.e("params", postDataParams.toString());

                Log.d("instream", "conn 1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                // conn.setRequestProperty("Acceptcharset", "en-us");
                // conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                // conn.setRequestProperty("charset", "EN-US");
                // conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");

                 conn.setReadTimeout(15000);
                 conn.setConnectTimeout(15000);
                 // conn.setRequestMethod("POST");
                 conn.setRequestMethod("GET");
                 conn.setDoInput(true);
                 conn.setDoOutput(true);
                Log.d("instream", "conn 2");

                  if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){

                      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                      StringBuffer sb = new StringBuffer();
                      String line = "";

                       while((line = in.readLine()) != null){
                           sb.append(line);
                           break;
                       }

                       in.close();
                       Log.d("INstream", sb.toString());
                       return sb.toString();
                  }
                  else{
                    Log.d("Instream", "error " + conn.getResponseCode());
                    return new String("false: " + conn.getResponseCode());

                  }
                // return null;
            }
            /* catch (MalformedURLException e) {
                 e.printStackTrace();
                // return null;
            } */
            catch(Exception e){
              return new String("Exception: " + e.getMessage());
            }

            // return "OK";
            // return null;
        }

        @Override
        protected void onPostExecute(String result) {
           // super.onPostExecute(o);
        }
    }

    public String getPostDataString(JSONObject params) throws Exception{

        return null;
    }


    public static boolean isOnline(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void showNoConnectionDialog(Context ctx1){

        final Context ctx = ctx1;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(true);
        builder.setMessage("no_connection - spoji se");
        builder.setTitle("no_connection wifi");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface dialog, int which)
           {
             // ctx.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
             //  ctx.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
               ctx.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
               // dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // context.startActivity(dialogIntent);
           }
       });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface dialog, int which)
           {
               Log.d("Instream", "cancel 01");
             return;
           }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() // ovaj CancelListener event se uopce ne odradjuje !?
        {
            public void onCancel(DialogInterface dialog) {
                Log.d("Instream", "cancel 02");
                return;
            }
        });

        builder.show();
    }

}
